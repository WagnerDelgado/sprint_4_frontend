import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddEditPacienteComponent } from '../paciente/add-edit-paciente/add-edit-paciente.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private dialog: MatDialog) { }

  openConfirmDialog(){
    this.dialog.open(AddEditPacienteComponent);
  }
}
