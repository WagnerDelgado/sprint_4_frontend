import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddListEmployeeSalaryComponent } from './add-list-employee-salary.component';

describe('AddListEmployeeSalaryComponent', () => {
  let component: AddListEmployeeSalaryComponent;
  let fixture: ComponentFixture<AddListEmployeeSalaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddListEmployeeSalaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddListEmployeeSalaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
