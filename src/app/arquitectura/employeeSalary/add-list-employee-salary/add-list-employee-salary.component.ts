import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { SalaryModel } from '../../models/salary';
import { SalaryManyService } from '../../services/salary-many.service';

@Component({
  selector: 'app-add-list-employee-salary',
  templateUrl: './add-list-employee-salary.component.html',
  styleUrls: ['./add-list-employee-salary.component.scss']
})
export class AddListEmployeeSalaryComponent implements OnInit {

  salaryModel: SalaryModel = new SalaryModel();
  //salaryListModel: SalaryListModel = new SalaryListModel();
  isLoading = false;
  //employeeModel: EmployeeModel;
  divFila1: boolean=true;
  divFila2: boolean=false;
  divFila3: boolean=false;
  divFila4: boolean=false;
  divFila5: boolean=false;
  divFila6: boolean=false;
  divFila7: boolean=false;
  divFila8: boolean=false;
  begin: Date;
  birthday: Date;
  constructor(private _router: Router, private _serviceSalary: SalaryManyService, private _fb: FormBuilder, private _activatedRoute: ActivatedRoute) {
    this.SalaryEmployeeForm();
  }

  ngOnInit(): void {
  }

  formGroupEmployeeSalary: FormGroup;

  SalaryEmployeeForm(salaryEmployee: SalaryModel = null) {
    this.formGroupEmployeeSalary = this._fb.group({
        id: [salaryEmployee != null ? salaryEmployee.id : 0],
        employeeName: [salaryEmployee != null ? salaryEmployee.employeeName : ''],
        employeeSurname: [salaryEmployee != null ? salaryEmployee.employeeSurname : ''],
        // year: [salary != null ? salary.year : 0],
        // month: [salary != null ? salary.month : 0],
        // baseSalary: [salary != null ? salary.baseSalary : 0],
        // employeeId: [salary != null ? salary.employeeId : ''],
        // productionBonus: [salary != null ? salary.productionBonus : 0],
        // compensationBonus: [salary != null ? salary.compensationBonus : 0],
        // commission: [salary != null ? salary.commission : 0],
        // contribution: [salary != null ? salary.commission : 0],
        ListaSalaries: this._fb.array([this.ListaSalariesGroup('','','','',0,'',0,0,this.begin,this.birthday,0,0,0,0,0,0,0)]),
    });
}

  BuscarEmpleadoByNombre(){
    let nombre = this.formGroupEmployeeSalary.get('employeeName').value;
    let apellido = this.formGroupEmployeeSalary.get('employeeSurname').value;
    console.log(nombre, apellido);
    this._serviceSalary.getSalaryByName(nombre, apellido).subscribe(res =>{
      console.log(res);
      if(res !== null){
        this.salaryModel = res;
      }else{
        Swal.fire({
          icon: 'error',
          title: 'No se ha encontrado al empleado, verifique si existe y/o ha ingresado datos en la busqueda!',
          text: `Error de busqueda!`,
          footer: ''
        })
      }

    }, error => {
      Swal.fire({
        icon: 'error',
        title: 'No se ha encontrado al empleado, verifique si existe y/o ha ingresado datos en la busqueda!',
        text: `Error de busqueda!`,
        footer: 'No se ha ingresado datos en la busqueda!'
      })

    })
  }

  mostrarFila1(){
    if(this.divFila1){
      this.divFila2=true;
    }
  }

  removerFila1(){
    this.divFila1=false;
  }

  mostrarFila2(){
    if(this.divFila2){
      this.divFila3=true;
    }
  }

  removerFila2(){
    this.divFila2=false;
  }

  mostrarFila3(){
    if(this.divFila3){
      this.divFila4=true;
    }
  }

  removerFila3(){
    this.divFila3=false;
  }

  mostrarFila4(){
    if(this.divFila4){
      this.divFila5=true;
    }
  }

  removerFila4(){
    this.divFila4=false;
  }

  mostrarFila5(){
    if(this.divFila5){
      this.divFila6=true;
    }
  }

  removerFila5(){
    this.divFila5=false;
  }


  mostrarFila6(){
    if(this.divFila6){
      this.divFila7=true;
    }
  }

  removerFila6(){
    this.divFila6=false;
  }

  mostrarFila7(){
    if(this.divFila7){
      this.divFila8=true;
    }
  }

  removerFila7(){
    this.divFila7=false;
  }

  ListaSalariesGroup(identificationNumber:string, employeeCode:string, employeeName:string, employeeSurname:string, officeId:number, divisionId: string, positionId:number,
    grade:number, begin:Date, birthday:Date,  year: number, month:number, baseSalary: number, productionBonus: number, compensationBonus: number, commission: number, contribution:number) {
    return this._fb.group({
      identificationNumber: [identificationNumber],
      employeeCode: [employeeCode],
      employeeName: [employeeName],
      employeeSurname:[employeeSurname],
      officeId:[officeId],
      divisionId:[divisionId],
      positionId:[positionId],
      grade:[grade],
      begin:[begin],
      birthday:[birthday],
      year: [year],
      month: [month],
      baseSalary: [baseSalary],
      productionBonus: [productionBonus],
      compensationBonus: [compensationBonus],
      commission: [commission],
      contribution: [contribution]
    });
  }

  get ListaSalaryArray(){
    return <FormArray>this.formGroupEmployeeSalary.get('ListaSalaries');
  }


  agregarListadoSalaries(){
    this.ListaSalaryArray.push(this.ListaSalariesGroup('','','','',0,'',0,0,this.begin,this.birthday,0,0,0,0,0,0,0));
  }

  removerListadoSalaries(index){
      this.ListaSalaryArray.removeAt(index);
  }

  Regresar(): void {
    this._router.navigate(['list-employee-salary']);
}


  Guardar(){
    let salarios: SalaryModel[];
    salarios = this.formGroupEmployeeSalary.value.ListaSalaries;
    salarios.forEach(obj => {
      obj.identificationNumber=this.salaryModel.identificationNumber;
      obj.employeeCode=this.salaryModel.employeeCode;
      obj.employeeName=this.salaryModel.employeeName;
      obj.employeeSurname=this.salaryModel.employeeSurname;
      obj.divisionId=this.salaryModel.divisionId;
      obj.officeId=this.salaryModel.officeId;
      obj.positionId=this.salaryModel.positionId;
      obj.begin = this.salaryModel.begin;
      obj.birthday = this.salaryModel.birthday;
      obj.grade = this.salaryModel.grade;
    });

    console.log(salarios);

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Agregar!",
        text: `Se va agregar una lista de salarios al sistema!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Guardar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
                    this._serviceSalary.postSalary(salarios).subscribe((res) => {
                      if(res){
                        console.log(res);
                        //this.cargar();
                        swalWithBootstrapButtons.fire("Guardado!", `El y/o los salarios, han sido guardados`, "success");
                        this.Regresar();
                      }
                },(error) => {
                  swalWithBootstrapButtons.fire("No se pudo guardar!", `El y/o la lista de salarios no se ha podido guardar`, "warning");
                } );

        }else{
          this.Regresar();
        }
      });
  }

}
