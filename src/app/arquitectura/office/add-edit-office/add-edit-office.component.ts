import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { OfficeModel } from '../../models/office';
import { OfficeService } from '../../services/office.service';

@Component({
  selector: 'app-add-edit-office',
  templateUrl: './add-edit-office.component.html',
  styleUrls: ['./add-edit-office.component.scss']
})
export class AddEditOfficeComponent implements OnInit {

  officeModel: OfficeModel = new OfficeModel();
  officeArray: OfficeModel []=[];

  constructor(private _router: Router, private _service: OfficeService, private _fb: FormBuilder, private _activatedRoute: ActivatedRoute) {
    this.OfficeForm();
  }

  ngOnInit(): void {
    this.cargarOffice();
  }

  cargarOffice(): void {
    this._activatedRoute.params.subscribe((params) => {
      let id = params["id"];
      if (id) {
        this._service.getOfficeById(id).subscribe(
          (res) => {
            console.log("res: ", res);
            this.officeModel = res;
            this.OfficeForm(this.officeModel);
          },
          (err) => {
            console.log("error: ", JSON.stringify(err));
          }
        );
      }
    });
  }

  officeCreateForm: FormGroup;

  OfficeForm(office: OfficeModel = null) {
    this.officeCreateForm = this._fb.group({
      id: [office !== null ? office.id : 0],
      name: [office !== null ? office.name : "", Validators.required],
    });
  }

  Regresar(): void {
    this._router.navigate(["/list-office"]);
  }

  get NameNoValido() {
    return (
      this.officeCreateForm.get("name").invalid &&
      this.officeCreateForm.get("name").touched
    );
  }


  Guardar(): void {
    let office = this.officeCreateForm.get('name').value;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Agregar!",
        text: `La Office ${office} se va agregar al sistema!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Guardar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.officeModel = this.officeCreateForm.value;
          if (this.officeModel.id === 0) {
            if (this.officeCreateForm.valid) {
                    this._service.postOffice(this.officeModel).subscribe((res) => {
                      console.log(res);
                        this.cargarOffice();
                        swalWithBootstrapButtons.fire("Guardado!", `La Office ${this.officeModel.name}, ha sido guardado`, "success");
                        this.Regresar();
                },(error) => {
                  swalWithBootstrapButtons.fire("No se pudo guardar!", `La Office ${this.officeModel.name}`, "warning");
                } );
          }
          }
        }else{
          this.Regresar();
        }
      });
  }

  Modificar(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });
    swalWithBootstrapButtons
      .fire({
        title: "Modificar!",
        text: "Seguro que desea modificar la division",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Modificar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.officeModel = this.officeCreateForm.value;
            if (this.officeCreateForm.valid) {
              this._service.putOffice(this.officeModel.id, this.officeModel).subscribe((res) => {
                this.officeArray = this.officeArray.filter((office) => office !== this.officeModel);
                this.cargarOffice();
                swalWithBootstrapButtons.fire(
                  "Modificado!",
                  `La Office ${this.officeModel.name}, ha sido modificado`,
                  "success"
                );
                this.Regresar();
            },(error) => {
              swalWithBootstrapButtons.fire("No se pudo modificar!", `La Division ${this.officeModel.name}`, "warning");
            });
        }else{
          this.Regresar();
        }

          }
      });
  }

}
