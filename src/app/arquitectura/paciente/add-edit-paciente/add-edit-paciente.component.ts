import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { PacienteModel } from '../../models/paciente';
import { PacienteService } from '../../services/paciente.service';

@Component({
  selector: 'app-add-edit-paciente',
  templateUrl: './add-edit-paciente.component.html',
  styleUrls: ['./add-edit-paciente.component.scss']
})
export class AddEditPacienteComponent implements OnInit {



  constructor(private dialogRef: MatDialogRef<AddEditPacienteComponent>,
    @Inject(MAT_DIALOG_DATA) private data:PacienteModel, private _service: PacienteService,
    private _activatedRoute: ActivatedRoute, private _fb: FormBuilder, private _router: Router){
      this.PacienteForm();
  }

  form: FormGroup;
  pacienteArray: PacienteModel[];
  paciente: PacienteModel = new PacienteModel();


  cargarPacientes(): void {
    this._activatedRoute.params.subscribe((params) => {
      let id = params["idPaciente"];
      if (id) {
        this._service.getPacienteById(id).subscribe(
          (res) => {
            console.log("res: ", res);
            this.paciente = res;
            this.PacienteForm(this.paciente);
          },
          (err) => {
            console.log("error: ", JSON.stringify(err));
          }
        );
      }
    });
  }

  PacienteForm(paciente: PacienteModel = null) {
    this.form = this._fb.group({
      idPaciente: [paciente !== null ? paciente.idPaciente : 0],
      cedula: [paciente !== null ? paciente.cedula : "", Validators.required],
      nombres: [paciente !== null ? paciente.nombres : "", Validators.required],
      apellidos: [paciente !== null ? paciente.apellidos : "", Validators.required],
      sexo: [paciente !== null ? paciente.sexo : "", Validators.required],
      telefono: [paciente !== null ? paciente.telefono : "", Validators.required],
      usuario: [paciente !== null ? paciente.usuario : "", Validators.required],
      password: [paciente !== null ? paciente.password : "", Validators.required],
    });
  }


  ngOnInit(): void {

    this.cargarPacientes();
    // this.form = new FormGroup({
    //   idMedico: new FormControl(),
    //   cedula: new FormControl(),
    //   nombres: new FormControl(),
    //   apellidos: new FormControl(),
    //   fotoUrl: new FormControl(),
    //   especialidad: new FormControl(),
    //   horario: new FormControl(),
    // });
  }

  Guardar(): void {
    let nombres = this.form.get('nombres').value;
    let apellidos = this.form.get('apellidos').value;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Agregar!",
        text: `El Médico ${nombres + ' ' + apellidos} se va agregar al sistema!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Guardar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.paciente = this.form.value;
          console.log(this.paciente);
          if (this.paciente.idPaciente == 0) {
            if (this.form.valid) {
                    this._service.postPaciente(this.paciente).subscribe((res) => {
                      console.log(res);
                        this.cargarPacientes();
                        swalWithBootstrapButtons.fire("Guardado!", `El Paciente ${nombres + ' ' + apellidos}, ha sido guardado`, "success");
                        this.Regresar();
                },(error) => {
                  swalWithBootstrapButtons.fire("No se pudo guardar!", `El Médico ${nombres + ' ' + apellidos}`, "warning");
                } );
          }
          }
        }else{
          this.Regresar();
        }
      });
  }

  // Modificar(): void {

  //   let nombresMedico = this.form.get('nombres').value;
  //   let apellidosMedico = this.form.get('apellidos').value;

  //   const swalWithBootstrapButtons = Swal.mixin({
  //       customClass: {
  //       confirmButton: "btn btn-success",
  //       cancelButton: "btn btn-danger",
  //       },
  //       buttonsStyling: false,
  //   });

  //   swalWithBootstrapButtons
  //       .fire({
  //       title: "Modificar!",
  //       text: `Desea modificar el medico ${nombresMedico + ' ' + apellidosMedico}?`,
  //       icon: "warning",
  //       showCancelButton: true,
  //       confirmButtonText: "Si, Modificar",
  //       cancelButtonText: "No, Cancelar!",
  //       reverseButtons: true,
  //       })
  //       .then((result) => {
  //       if (result.isConfirmed) {
  //           this.medico = this.form.value;
  //           if (this.form.valid) {
  //               this._service.putMedico(this.medico).subscribe((res) => {
  //               if(res){
  //               //this.medicoArray = this.medicoArray.filter((medico) => medico !== this.medico);
  //               this.cargarMedicos();
  //               swalWithBootstrapButtons.fire(
  //                   "Modificado!",
  //                   `El medico ${nombresMedico + ' ' + apellidosMedico}, ha sido modificado`,
  //                   "success"
  //               );
  //               this.Regresar();
  //               }else{
  //               swalWithBootstrapButtons.fire(
  //                   "No se pudo Modificar!",
  //                   `El medico ${nombresMedico + ' ' + apellidosMedico}, ya existe y/o se encuentra vinculado!`,
  //                   "warning"
  //               );
  //               }
  //           });
  //       }else{
  //           this.Regresar();
  //       }

  //           }
  //       });
  //   }

  Regresar(): void {
    this._router.navigate(["/list-medico"]);
  }

  Cerrar(){
    this.dialogRef.close();
  }
}
