import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DivisionModel } from '../../models/division';
import { DivisionService } from '../../services/division.service';

@Component({
  selector: 'app-list-division',
  templateUrl: './list-division.component.html',
  styleUrls: ['./list-division.component.scss'],
})
export class ListDivisionComponent implements OnInit {
  divisionModel: DivisionModel;
  divisionArray: DivisionModel[] = [];
  displayedColumns: string[] = ["id", "name", "acciones"];
  dataSource = new MatTableDataSource();
  isLoading = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private _router: Router, private _service: DivisionService) {
    this.CargarTabla();
  }

  Buscar(filtrar: string): void {
    this.dataSource.filter = filtrar.trim().toLowerCase();
  }

  CargarTabla() {
    this.dataSource = new MatTableDataSource();
    this.isLoading = true;
    this._service.getDivision().subscribe(
      (res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.isLoading = false;
      },
      (error) => (this.isLoading = false)
    );
  }

  ngOnInit(): void {}

  Eliminar(division: DivisionModel): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: 'Esta seguro?',
        text: 'Seguro que desea eliminar la division?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this._service.deleteDivision(division.id).subscribe(
            (res) => {

                this.divisionArray = this.divisionArray.filter((division) => division !== division);
                this.CargarTabla();
                swalWithBootstrapButtons.fire(
                  'Eliminado!',
                  `La division ha sido eliminado!`,
                  'success'
                );

            },
            (error) => {
              swalWithBootstrapButtons.fire(
                'No se pudo Eliminar!',
                `La division ya se ha generado anteriormente!`,
                'warning'
              );
            }
          );
        }
      });
  }
}
