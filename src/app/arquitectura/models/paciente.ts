export class PacienteModel{
  idPaciente:number;
  cedula:string;
  nombres: string;
  apellidos: string;
  sexo: string;
  telefono: string;
  usuario: string;
  password: string;
}
