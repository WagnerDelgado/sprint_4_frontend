import { DivisionModel } from "./division";
import { OfficeModel } from "./office";
import { PositionModel } from "./position";

export class EmployeeModel {
  id: number;
  identificationNumber: string;
  employeeCode: string;
  employeeName: string;
  employeeSurname: string;
  officeId: number;
  office?: OfficeModel;
  divisionId: number;
  division?: DivisionModel;
  positionId: number;
  position?: PositionModel;
  grade: number;
  begin: Date;
  birthday: Date;
}
