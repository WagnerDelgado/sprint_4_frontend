import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SalaryModel } from '../models/salary';

const header = {
  'Content-Type': 'application/json',
};

const httpOptions = {
  headers: header,
};

@Injectable({
  providedIn: 'root'
})
export class SalaryManyService {

  private url = environment.api + '/api/Salary';
  private urlList = environment.api + '/api/SalaryList';
  private urlPost = environment.api + '/api/SalaryList';
  private urlDelete = environment.api + '/api/Salary';

  constructor(private _http: HttpClient) { }

  getSalary(): Observable<any> {
    return this._http.get(this.url, httpOptions);
  }

  getSalaryById(id: number): Observable<any> {
    return this._http.get(`${this.url}/${id}`, httpOptions);
  }

  getSalaryByName(employeeName: string, employeeSurname: string): Observable<any> {
    return this._http.get(`${this.urlList}?employeeName=${employeeName}&employeeSurname=${employeeSurname}`, httpOptions);
  }

  postSalary(salaries: SalaryModel[]): Observable<any> {
    return this._http.post(this.urlPost, salaries, httpOptions);
  }

  // putEmployee(id: number, salary: SalaryModel[]): Observable<any> {
  //   return this._http.put(`${this.urlPut}/${id}`, employee, httpOptions);
  // }

  deleteSalary(id: number) {
    return this._http.delete(`${this.urlDelete}/${id}`, httpOptions);
  }
}
