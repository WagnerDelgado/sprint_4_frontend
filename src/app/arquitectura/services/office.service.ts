import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { OfficeModel } from '../models/office';

const header = {
  'Content-Type': 'application/json',
};

const httpOptions = {
  headers: header,
};

@Injectable({
  providedIn: 'root'
})
export class OfficeService {

  constructor(private _http: HttpClient) { }

  private url = environment.api + '/api/Office';
  private urlPost = environment.api + '/api/Office';
  private urlPut = environment.api + '/api/Office';
  private urlDelete = environment.api + '/api/Office';


  getOffice(): Observable<any> {
    return this._http.get(this.url, httpOptions);
  }

  getOfficeById(id: number): Observable<any> {
    return this._http.get(`${this.url}/${id}`, httpOptions);
  }

  postOffice(office: OfficeModel): Observable<any> {
    return this._http.post(this.urlPost, office, httpOptions);
  }

  putOffice(id: number, office: OfficeModel): Observable<any> {
    return this._http.put(`${this.urlPut}/${id}`, office, httpOptions);
  }

  deleteOffice(id: number) {
    return this._http.delete(`${this.urlDelete}/${id}`, httpOptions);
  }
}
