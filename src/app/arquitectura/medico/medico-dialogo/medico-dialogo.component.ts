import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { EspecialidadModel } from '../../models/especialidad';
import { HorarioModel } from '../../models/horario';
import { MedicoModel } from '../../models/medico';
import { MedicoService } from '../../services/medico.service';

@Component({
  selector: 'app-medico-dialogo',
  templateUrl: './medico-dialogo.component.html',
  styleUrls: ['./medico-dialogo.component.scss']
})
export class MedicoDialogoComponent implements OnInit {

  especialidadArray: EspecialidadModel[];
  horarioArray: HorarioModel[];

  medico: MedicoModel;
  constructor(private dialogRef: MatDialogRef<MedicoDialogoComponent>,
    @Inject(MAT_DIALOG_DATA) private data:MedicoModel,
    private _service: MedicoService){
  }

  ngOnInit(): void {
    this.medico = { ...this.data };
    console.log(this.data);


  }

  // operar(){
  //   if(this.medico !== null && this.medico.idMedico > 0){
  //     //modificar
  //     this._service.put(this.medico).subscribe(() =>{
  //       this._service.getAll().subscribe(res =>{
  //         this._service.setMedicoCambio(res);
  //         this._service.setMensajeCambio("Se modifico");
  //       });
  //     });
  //   }else{
  //     //agregar
  //     this._service.post(this.medico).pipe(switchMap( () => {
  //       return this._service.getAll();
  //     })).subscribe((data:any) => {
  //       this._service.setMedicoCambio(data);
  //       this._service.setMensajeCambio("Se registro");
  //     });
  //   }
  // }

  cerrar(){
    this.dialogRef.close();
  }

}


