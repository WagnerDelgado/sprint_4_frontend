import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicoEspecialComponent } from './medico-especial.component';

describe('MedicoEspecialComponent', () => {
  let component: MedicoEspecialComponent;
  let fixture: ComponentFixture<MedicoEspecialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicoEspecialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicoEspecialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
