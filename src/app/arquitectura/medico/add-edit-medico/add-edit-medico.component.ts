import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { EspecialidadModel } from '../../models/especialidad';
import { HorarioModel } from '../../models/horario';
import { MedicoModel } from '../../models/medico';
import { MedicoService } from '../../services/medico.service';

@Component({
  selector: 'app-add-edit-medico',
  templateUrl: './add-edit-medico.component.html',
  styleUrls: ['./add-edit-medico.component.scss']
})
export class AddEditMedicoComponent implements OnInit {

  medicoModel: MedicoModel = new MedicoModel();
  medicoArray: MedicoModel []=[];
  especialidadArray: EspecialidadModel[];
  horarioArray: HorarioModel[];
  especialidadSeleccionada: number;

  constructor(private _router: Router, private _service: MedicoService, private _fb: FormBuilder, private _activatedRoute: ActivatedRoute) {
    this.MedicoForm();
  }

  ngOnInit(): void {
    this.cargarMedicos();

    this._service.getEspecialidad().subscribe((response) => {
      this.especialidadArray = response;
      console.log(response);
    });

    this._service.getHorario().subscribe((response) => {
      this.horarioArray = response;
      console.log(response);
    });
  }

  crearTabla(data: MedicoModel[]){

  }

  cargarMedicos(): void {
    this._activatedRoute.params.subscribe((params) => {
      let id = params["idMedico"];
      if (id) {
        this._service.getMedicoById(id).subscribe(
          (res) => {
            console.log("res: ", res);
            this.medicoModel = res;
            this.MedicoForm(this.medicoModel);
          },
          (err) => {
            console.log("error: ", JSON.stringify(err));
          }
        );
      }
    });
  }

  medicoCreateForm: FormGroup;

  MedicoForm(medico: MedicoModel = null) {
    this.medicoCreateForm = this._fb.group({
      idMedico: [medico !== null ? medico.idMedico : 0],
      cedula: [medico !== null ? medico.cedula : "", Validators.required],
      nombres: [medico !== null ? medico.nombres : "", Validators.required],
      apellidos: [medico !== null ? medico.apellidos : "", Validators.required],
      //idEspecialidad: [medico !== null ? medico.idEspecialidad : ""],
      especialidad: [medico !== null ? medico.especialidad : null],
      //idHorario: [medico !== null ? medico.idHorario : ""],
      horario: [medico !== null ? medico.horario : null]
    });
  }

  Regresar(): void {
    this._router.navigate(["/list-medico"]);
  }

  seleccionarCombo(e){
    console.log(e);
  }


  Guardar(): void {
    let nombresMedico = this.medicoCreateForm.get('nombres').value;
    let apellidosMedico = this.medicoCreateForm.get('apellidos').value;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Agregar!",
        text: `El Médico ${nombresMedico + ' ' + apellidosMedico} se va agregar al sistema!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Guardar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.medicoModel = this.medicoCreateForm.value;
          if (this.medicoModel.idMedico === 0) {
            if (this.medicoCreateForm.valid) {
                    this._service.postMedico(this.medicoModel).subscribe((res) => {
                      console.log(res);
                        this.cargarMedicos();
                        swalWithBootstrapButtons.fire("Guardado!", `El Médico ${nombresMedico + ' ' + apellidosMedico}, ha sido guardado`, "success");
                        this.Regresar();
                },(error) => {
                  swalWithBootstrapButtons.fire("No se pudo guardar!", `El Médico ${nombresMedico + ' ' + apellidosMedico}`, "warning");
                } );
          }
          }
        }else{
          this.Regresar();
        }
      });
  }


  Modificar(): void {
    let nombresMedico = this.medicoCreateForm.get('nombres').value;
    let apellidosMedico = this.medicoCreateForm.get('apellidos').value;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });
    swalWithBootstrapButtons
      .fire({
        title: "Modificar!",
        text: `El Médico ${nombresMedico + ' ' + apellidosMedico} se va a modificar al sistema!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Modificar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.medicoModel = this.medicoCreateForm.value;
            if (this.medicoCreateForm.valid) {
              this._service.putMedico(this.medicoModel).subscribe((res) => {
                this.medicoArray = this.medicoArray.filter((medico) => medico !== this.medicoModel);
                this.cargarMedicos();
                swalWithBootstrapButtons.fire(
                  "Modificado!",
                  `El Médico ${nombresMedico + ' ' + apellidosMedico}, ha sido modificado`,
                  "success"
                );
                this.Regresar();
            },(error) => {
              swalWithBootstrapButtons.fire("No se pudo modificar!", `El Médico ${nombresMedico + ' ' + apellidosMedico}`, "warning");
            });
        }else{
          this.Regresar();
        }

          }
      });
  }

}
