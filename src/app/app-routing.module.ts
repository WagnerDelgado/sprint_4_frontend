import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEditDivisionComponent } from './arquitectura/division/add-edit-division/add-edit-division.component';
import { ListDivisionComponent } from './arquitectura/division/list-division/list-division.component';
import { AddEmployeeSalaryComponent } from './arquitectura/employeeSalary/add-employee-salary/add-employee-salary.component';
import { AddListEmployeeSalaryComponent } from './arquitectura/employeeSalary/add-list-employee-salary/add-list-employee-salary.component';
import { ListEmployeeSalaryComponent } from './arquitectura/employeeSalary/list-employee-salary/list-employee-salary.component';
import { AddEditEspecialidadComponent } from './arquitectura/especialidad/add-edit-especialidad/add-edit-especialidad.component';
import { ListEspecialidadComponent } from './arquitectura/especialidad/list-especialidad/list-especialidad.component';
import { AddEditMedicoComponent } from './arquitectura/medico/add-edit-medico/add-edit-medico.component';
import { DetalleFotoComponent } from './arquitectura/medico/detalle-foto/detalle-foto.component';
import { ListMedicoComponent } from './arquitectura/medico/list-medico/list-medico.component';
import { MedicoEspecialComponent } from './arquitectura/medico/medico-especial/medico-especial.component';
import { NavbarComponent } from './arquitectura/navbar/navbar.component';
import { AddEditOfficeComponent } from './arquitectura/office/add-edit-office/add-edit-office.component';
import { ListOfficeComponent } from './arquitectura/office/list-office/list-office.component';
import { AddEditPacienteComponent } from './arquitectura/paciente/add-edit-paciente/add-edit-paciente.component';
import { ListPacienteComponent } from './arquitectura/paciente/list-paciente/list-paciente.component';
import { AddEditPositionComponent } from './arquitectura/position/add-edit-position/add-edit-position.component';
import { ListPositionComponent } from './arquitectura/position/list-position/list-position.component';
import { AddEditSalaryComponent } from './arquitectura/salary/add-edit-salary/add-edit-salary.component';
import { ListSalaryComponent } from './arquitectura/salary/list-salary/list-salary.component';

const routes: Routes = [
  {path: '', component: NavbarComponent},
  {path: 'list-especialidad', component: ListEspecialidadComponent},
  {path: 'add-edit-especialidad', component: AddEditEspecialidadComponent},
  {path: 'add-edit-especialidad/:idEspecialidad', component: AddEditEspecialidadComponent},
  {path: 'list-medico', component: ListMedicoComponent},
  {path: 'add-edit-medico', component: AddEditMedicoComponent},
  {path: 'add-edit-medico/:idMedico', component: AddEditMedicoComponent},
  {path: 'medico-especial', component: MedicoEspecialComponent},
  {path: 'medico-especial/:idMedico', component: MedicoEspecialComponent},
  {path: 'detalle-foto/:idMedico', component: DetalleFotoComponent},
  {path: 'list-paciente', component: ListPacienteComponent},
  {path: 'add-edit-paciente', component: AddEditPacienteComponent},
  {path: 'add-edit-paciente/:idPaciente', component: AddEditPacienteComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
