import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListOfficeComponent } from './arquitectura/office/list-office/list-office.component';
import { AddEditOfficeComponent } from './arquitectura/office/add-edit-office/add-edit-office.component';
import { ListDivisionComponent } from './arquitectura/division/list-division/list-division.component';
import { AddEditDivisionComponent } from './arquitectura/division/add-edit-division/add-edit-division.component';
import { AddEditPositionComponent } from './arquitectura/position/add-edit-position/add-edit-position.component';
import { ListPositionComponent } from './arquitectura/position/list-position/list-position.component';
import { NavbarComponent } from './arquitectura/navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ListSalaryComponent } from './arquitectura/salary/list-salary/list-salary.component';
import { AddEditSalaryComponent } from './arquitectura/salary/add-edit-salary/add-edit-salary.component';
import { ListEmployeeSalaryComponent } from './arquitectura/employeeSalary/list-employee-salary/list-employee-salary.component';
import { AddEmployeeSalaryComponent } from './arquitectura/employeeSalary/add-employee-salary/add-employee-salary.component';
import { AddListEmployeeSalaryComponent } from './arquitectura/employeeSalary/add-list-employee-salary/add-list-employee-salary.component';
import { ListEspecialidadComponent } from './arquitectura/especialidad/list-especialidad/list-especialidad.component';
import { AddEditEspecialidadComponent } from './arquitectura/especialidad/add-edit-especialidad/add-edit-especialidad.component';
import { ListMedicoComponent } from './arquitectura/medico/list-medico/list-medico.component';
import { AddEditMedicoComponent } from './arquitectura/medico/add-edit-medico/add-edit-medico.component';
import { DetalleFotoComponent } from './arquitectura/medico/detalle-foto/detalle-foto.component';
import { MedicoDialogoComponent } from './arquitectura/medico/medico-dialogo/medico-dialogo.component';
import { MedicoEspecialComponent } from './arquitectura/medico/medico-especial/medico-especial.component';
import { ListPacienteComponent } from './arquitectura/paciente/list-paciente/list-paciente.component';
import { AddEditPacienteComponent } from './arquitectura/paciente/add-edit-paciente/add-edit-paciente.component';



@NgModule({
  declarations: [
    AppComponent,
    ListOfficeComponent,
    AddEditOfficeComponent,
    ListDivisionComponent,
    AddEditDivisionComponent,
    AddEditPositionComponent,
    ListPositionComponent,
    NavbarComponent,
    ListSalaryComponent,
    AddEditSalaryComponent,
    ListEmployeeSalaryComponent,
    AddEmployeeSalaryComponent,
    AddListEmployeeSalaryComponent,
    ListEspecialidadComponent,
    AddEditEspecialidadComponent,
    ListMedicoComponent,
    AddEditMedicoComponent,
    DetalleFotoComponent,
    MedicoDialogoComponent,
    MedicoEspecialComponent,
    ListPacienteComponent,
    AddEditPacienteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],

  exports:[],
  providers: [{provide: LOCALE_ID, useValue: 'es'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
